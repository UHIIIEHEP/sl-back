import {swShopRoute} from "./src/api/shop/shop.swagger";
import {swUnitRoute} from "./src/api/unit/unit.swagger";
import {swShopReceiptRoute} from "./src/api/shop_receipt/shop_receipt.swagger";
import {swStorageRoute} from "./src/api/storage/storage.swagger";

const swagger = {
  openapi: '3.0.0',
  info: {
    title: 'Express API for ShopList',
    version: '1.0.0',
    description: 'The REST API for ShopList Panel service'
  },
  servers: [
    {
      url: `http://${process.env.APP_HOST}:${Number(process.env.APP_PORT) || 5000}`,
      description: 'Development server'
    }
  ],
  paths: {
    ...swShopRoute,
    ...swUnitRoute,
    ...swShopReceiptRoute,
    ...swStorageRoute,
  },
}

export default swagger
export const COLLECTION_SHOP = 'shop';
export const COLLECTION_UNIT = 'unit';
export const COLLECTION_SHOP_RECEIPT = 'shop_receipt';
export const COLLECTION_STORAGE = 'storage';

const {
    DB_HOST,
    DB_PORT,
    DB_DATABASE,
} = process.env

export const mongo = {
    url: `mongodb://${DB_HOST}:${Number(DB_PORT)}/${DB_DATABASE}`
}

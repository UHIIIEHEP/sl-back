const response = (req, res, body) => {
  console.log({
    address: req.path,
    status: 'ok',
    body,
  })

  res.send({
    status: 'ok',
    body,
  })
}

const responseError = (err, req, res, next) => {
  const statusCode = 500;
  console.log({
    address: req.path,
    status: 'error',
    code: statusCode,
    ...err,
  })

  res
    .status(statusCode)
    .send({
    path: req.path,
    message: err.message,
    ...err,
  })
}

const sendResponse = (res, req, data) => {
  console.log({
    status: 'ok',
    level: 30,
    method: req.method,
    url: req.url,
    body: req.body,
    params: req.params,
    query: req.query,
    result: JSON.stringify(data),
  })

  res.send({
    status: 'ok',
    result: data,
  })
}

export {
  response,
  responseError,
  sendResponse,
}

import {schemaShopReceiptCreate} from "./shop_receipt.shema";

const swShopReceiptCreate = {
  "summary": "Shop receipt create",
  "tags": [
    "shop receipt"
  ],
  "requestBody": {
    "content": {
      "application/json": {
        "schema": {
          ...schemaShopReceiptCreate
        }
      }
    }
  },
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

const swShopReceiptList = {
  "summary": "Shop receipt list",
  "tags": [
    "shop receipt"
  ],
  "parameters": [
    {
      "name": "_id",
      "in": "query",
      "schema": {
        "type": "string"
      },
      "required": false
    },
    {
      "name": "category",
      "in": "query",
      "schema": {
        "type": "boolean"
      },
      "required": false
    },
    {
      "name": "date_from",
      "in": "query",
      "schema": {
        "type": "string"
      },
      "required": false
    },
    {
      "name": "date_to",
      "in": "query",
      "schema": {
        "type": "string"
      },
      "required": false
    },
  ],
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

export const swShopReceiptRoute = {
  "/shop/receipt/list": {
    "get": {
      ...swShopReceiptList
    },
  },
  "/shop/receipt/create": {
    "post": {
      ...swShopReceiptCreate
    }
  }
}
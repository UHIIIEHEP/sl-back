import {IProduct, IShopReceiptResponse} from "./shop_receipt.interface";
import {COLLECTION_SHOP_RECEIPT} from "../../constant/collection_name";
import {ObjectId} from 'mongodb';

const shopReceiptService = () => {
  return ({
    list: async (database, payload): Promise<IShopReceiptResponse[]> => {
      const {
        _id,
        category,
        date_from,
        date_to,
      } = payload;

      let filter = null;

      filter = !!_id ? Object.assign(filter||{}, {_id: new ObjectId(_id)}) : filter;
      filter = !!category ? Object.assign(filter||{}, {category}) : filter;
      filter = !!date_from ? Object.assign(filter||{}, {date: {$gte: date_from}}) : filter;
      filter = !!date_to ? Object.assign(filter||{}, {date: {$lte: date_to}}) : filter;

      const shopReceipt = await database
        .collection(COLLECTION_SHOP_RECEIPT)
        .find(filter)
        .toArray();

      const receipt = shopReceipt.map(item => {
        const product = item.product.map(itemProduct => {
          item.total_price += Number(itemProduct.price);
          return {
            ...itemProduct,
            unit: itemProduct.unit,
          }
        })
        return {
          ...item,
          product,
        }
      })

      return receipt;
    },

    create: async (database, payload): Promise<IShopReceiptResponse> => {

      const product = payload.product.map((item: IProduct) => {
        return {
          name: item.name,
          price: item.price,
          quantity: item.quantity,
          unit: item.unit,
          strategy: item.strategy,
          consumed: null,
        }
      })

      let shopReceiptId = await database.collection(COLLECTION_SHOP_RECEIPT).insertOne({
        date: payload.date || new Date().toISOString().slice(0,10),
        category: payload.category,
        product,
        total_price: payload.total_price,
      });

      const shopReceipt = await database.collection(COLLECTION_SHOP_RECEIPT).findOne({_id: shopReceiptId.insertedId});

      return shopReceipt;
    }
  })
}

export default shopReceiptService();

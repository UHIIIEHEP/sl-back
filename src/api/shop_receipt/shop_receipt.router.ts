import express from 'express'
import {mongo} from "../../config/mongo";
import shop_receiptController from "./shop_receipt.controller";
const MongoClient = require('mongodb').MongoClient;

const receiptRouter = express.Router()

MongoClient.connect(mongo.url, async (err, client) => {
  if (err) return console.log({err});

  const database = client.db(process.env.DB_DATABASE);

  receiptRouter.get('/list', shop_receiptController(database).list);
  receiptRouter.post('/create', shop_receiptController(database).create);
});

export default receiptRouter;

import {CategoryEnum} from "../shop/shop.interface";

const joi = require('joi')
const j2s = require('joi-to-swagger')

// Joi
const product = joi.object().keys({
  name: joi.string().required().example('Старосормовский хлеб'),
  price: joi.number().required().example(24.99),
  quantity: joi.number().required().example(1),
  strategy: joi.boolean().example(false),
  unit: joi.string().example('piece'),
})

// const shop = joi.object().keys({
//   _id: joi.string().required(),
//   name: joi.string().required(),
// })

const shopReceiptCreate = joi.object().keys({
  category: joi.string().allow('NO_CATEGORY').example('NO_CATEGORY'),
  date: joi.string().allow(null).example('2021-12-12'),
  total_price: joi.number().example(24.99),
  product: joi.array().items(product)
})

const shopReceiptList = joi.object().keys({
  _id: joi.string(),
  category: joi.string(),
  date_from: joi.string(),
  date_to: joi.string(),
})
// end of Joi

const schemaShopReceiptCreate = j2s(shopReceiptCreate).swagger

export {
  shopReceiptCreate,
  shopReceiptList,
  schemaShopReceiptCreate,
}

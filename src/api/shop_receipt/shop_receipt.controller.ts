import {shopReceiptList, shopReceiptCreate} from "./shop_receipt.shema";
import shopReceiptService from "./shop_receipt.service";
import {sendResponse} from "../../helper";

const shop_receiptController = (database) => {
  return ({
    list: async (req, res, next) => {
      try{
        await shopReceiptList.validateAsync(req.query)
        const shopReceipt = await shopReceiptService.list(database, req.query);

        sendResponse(res, req, {shopReceipt});
      } catch (err) {
        next(err);
      }
    },

    create: async (req, res, next) => {
      try {
        await shopReceiptCreate.validateAsync(req.body)

        const shopReceiptList = await  shopReceiptService.create(database, req.body)

        sendResponse(res, req, {shopReceiptList});
      } catch (err) {
        next(err);
      }
    },
  })
}

export default shop_receiptController;

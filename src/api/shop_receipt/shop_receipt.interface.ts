

interface IProduct {
  name: string,
  price: number,
  quantity: number,
  unit: string,
  strategy?: boolean,
  consumed?: number
}

interface IShopReceiptCreate {
  _id: string,
  date: string,
  total_price: number,
  product: IProduct[],
}

interface IShopReceiptResponse extends IShopReceiptCreate{
  _id: string,
  category: string,
}

export {
  IShopReceiptCreate,
  IShopReceiptResponse,
  IProduct,
}

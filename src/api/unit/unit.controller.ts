import {sendResponse} from "../../helper";
import {shopList} from "../shop/shop.shema";
import unitService from "./unit.service";

const unitController = () => {
  return ({
    list: async (req, res, next) => {
      try {
        const unit = await unitService.list();

        sendResponse(res, req, {unit});
      } catch(err) {
        next(err);
      }
    },
  })
}

export default unitController;

const unitService = () => {
  return ({
    list: async (): Promise<string[]> => [
      'gram',
      'kilogram',
      'liter',
      'piece',
    ],
  })
}

export default unitService();

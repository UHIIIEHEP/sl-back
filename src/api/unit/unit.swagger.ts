
const swUnitList = {
  "summary": "Unit list",
  "tags": [
    "unit"
  ],
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

export const swUnitRoute = {
  "/unit/list": {
    "get": {
      ...swUnitList
    },
  },
}
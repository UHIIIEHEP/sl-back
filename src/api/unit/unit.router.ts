import express from 'express'
import unitController from "./unit.controller";

const unitRouter = express.Router()

unitRouter.get('/list', unitController().list);

export default unitRouter;

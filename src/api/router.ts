const express = require('express');
import shop from "./shop/shop.router";
import unit from "./unit/unit.router";
import shop_receipt from "./shop_receipt/shop_receipt.router";
import storage from "./storage/storage.router";

const router = express.Router({ mergeParams: true });

router.use('/shop', shop);
router.use('/unit', unit);
router.use('/shop/receipt', shop_receipt);
router.use('/storage', storage);

export default router;

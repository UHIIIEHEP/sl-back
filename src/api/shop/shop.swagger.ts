import {schemaShopCreate} from "./shop.shema";

const swShopCreate = {
  "summary": "Shop create",
  "tags": [
    "shop"
  ],
  "requestBody": {
    "content": {
      "application/json": {
        "schema": {
          ...schemaShopCreate
        }
      }
    }
  },
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

const swShopList = {
  "summary": "Shop list",
  "tags": [
    "shop"
  ],
  "parameters": [
    {
      "name": "_id",
      "in": "query",
      "schema": {
        "type": "string"
      },
      "required": false
    }
  ],
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

const swShopCategoryList = {
  "summary": "Shop category list",
  "tags": [
    "shop"
  ],
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  }
}

export const swShopRoute = {
  "/shop/list": {
    "get": {
      ...swShopList
    },
  },
  "/shop/create": {
    "post": {
      ...swShopCreate
    }
  },
  "/shop/category/list": {
    "get": {
      ...swShopCategoryList
    }
  },
}
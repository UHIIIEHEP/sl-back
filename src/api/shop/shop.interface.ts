interface IShop {
  name: string,
  alias: string,
}

interface IShopResponse extends IShop{
  _id: string,
}

export {
  IShop,
  IShopResponse,
};

export enum CategoryEnum {
  'NO_CATEGORY' = 'Без категории',
  'PRODUCT' = 'Продукты',
  'PHARMACY' = 'Аптека',
}
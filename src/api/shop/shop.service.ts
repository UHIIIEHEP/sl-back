import {IShopResponse} from "./shop.interface";
import {COLLECTION_SHOP} from "../../constant/collection_name";
import {ObjectId} from "mongodb";

const shopService = () => {
  return ({
    list: async (database, payload): Promise<IShopResponse[]> => {
      try {
        const {_id} = payload;
        let filter = null;

        filter = !!_id ? Object.assign(filter||{}, {_id: new ObjectId(_id)}) : filter;

        const shop = await database
          .collection(COLLECTION_SHOP)
          .find(filter)
          .toArray();

        return shop;
      } catch(err) {
        throw new Error(err)
      }
    },

    create: async (database, payload): Promise<IShopResponse> => {
      const unique = await database.collection(COLLECTION_SHOP).find({ //вставить или обновить
        $or: [
          {name: payload.name},
          {alias: payload.alias},
        ]
      }).toArray();

      if (unique.length !== 0) {
        throw ('shop_is_not_unique')
      }
      const shopId = await database.collection(COLLECTION_SHOP).insertOne(payload);
      const shop = await database.collection(COLLECTION_SHOP).findOne({_id: shopId.insertedId});
      return shop;
    },

    categoryList: async (): Promise<string[]> => [
      'no_category',
      'product',
      'pharmacy',
    ],
  })
}

export default shopService();

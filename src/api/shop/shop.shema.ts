const joi = require('joi')
const j2s = require('joi-to-swagger')

// Joi
const shopCreate = joi.object().keys({
  name: joi.string().required(),
  alias: joi.string().required(),
})

const shopList = joi.object().keys({
  _id: joi.string(),
})
// end of Joi

const schemaShopCreate = j2s(shopCreate).swagger;

export {
  shopCreate,
  shopList,
  schemaShopCreate,
}

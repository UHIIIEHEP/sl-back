import {shopCreate, shopList} from "./shop.shema";

import {Request} from 'express';
import {sendResponse} from "../../helper";
import shopService from "./shop.service";

const shopController = (database) => {
  return ({
    list: async (req: Request, res, next): Promise<void> => {
      try{

        await shopList.validateAsync(req.query)

        const shop = await shopService.list(database, req.query);

        sendResponse(res, req, {shop});
      } catch (err) {
        next({err});
      }
    },

    create: async (req, res, next): Promise<void> => {
      try {
        await shopCreate.validateAsync(req.body)

        const shop = await shopService.create(database, req.body);

        sendResponse(res, req, {shop});
      } catch (err) {
        next(err);
      }
    },

    categoryList: async (req, res, next): Promise<void> => {
      try {

        const category = await shopService.categoryList();

        sendResponse(res, req, {category});
      } catch (err) {
        next(err);
      }
    },
  })
}

export default shopController;

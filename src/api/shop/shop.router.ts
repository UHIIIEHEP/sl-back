import express from 'express'
import {mongo} from "../../config/mongo";
const MongoClient = require('mongodb').MongoClient;
import shopController from "./shop.controller";

const shopRouter = express.Router()

MongoClient.connect(mongo.url, async (err, client) => {
  if (err) return console.log({err});
  const database = client.db(process.env.DB_DATABASE);

  shopRouter.get('/list', shopController(database).list);
  shopRouter.post('/create',  shopController(database).create);

  shopRouter.get('/category/list',  shopController(database).categoryList);
});

export default shopRouter;

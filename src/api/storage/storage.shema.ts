
const joi = require('joi')


const storageList = joi.object().keys({
  strategy: joi.boolean(),
})

export {
  storageList,
}

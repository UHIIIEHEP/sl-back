import {COLLECTION_SHOP_RECEIPT, COLLECTION_STORAGE} from "../../constant/collection_name";
import {ObjectId} from "mongodb";

const storageService = () => {
  return ({
    list: async (database, payload): Promise<any> => {
      try {
        const {strategy} = payload;
        let filter = null;

        filter = strategy === 'true' ?
          Object.assign(filter||{}, {'product.strategy': true}) :
          Object.assign(filter||{}, {$or: [{'product.strategy': false}, {'product.strategy': null}]});

        const storage = await database
          .collection(COLLECTION_SHOP_RECEIPT)
          .find(filter)
          .toArray();

        return storage;
      } catch(err) {
        throw new Error(err)
      }
    }
  })
}

export default storageService();

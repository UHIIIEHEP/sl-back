import express from "express";
import {mongo} from "../../config/mongo";
import storageController from "./storage.controller";
const MongoClient = require('mongodb').MongoClient;

const storageRouter = express.Router()

MongoClient.connect(mongo.url, async (err, client) => {
  if (err) return console.log({err});
  const database = client.db(process.env.DB_DATABASE);

  storageRouter.get('/list', storageController(database).list);
});

export default storageRouter;
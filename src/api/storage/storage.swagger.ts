

const swStorageList = {
  "summary": "Storage create",
  "tags": [
    "storage"
  ],
  "parameters": [
    {
      "name": "strategy",
      "in": "query",
      "schema": {
        "type": "string"
      },
      "required": false
    },
  ],
  "responses": {
    "200": {
      "description": "Done"
    },
    "default": {
      "description": "Error message"
    }
  },
}

export const swStorageRoute = {
  "/storage/list": {
    "get": {
      ...swStorageList
    }
  }
}

import {Request} from "express";
import {sendResponse} from "../../helper";
import storageService from "./storage.service";


const storageController = (database) => {
  return ({
    list: async (req: Request, res, next): Promise<any> => {
      try {

        const storage = await storageService.list(database, req.query);

        sendResponse(res, req, {storage});
      } catch (err) {
        next({err});
      }
    },
  })
}

export default storageController;

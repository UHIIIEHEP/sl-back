import express, {Response, Request} from 'express';
import cors from 'cors';
require('dotenv').config();
// const cors = require('cors');
// const cors = require('cors');
import swaggerUI = require('swagger-ui-express')
import swDocument from './openapi'

import router from './src/api/router';

const bodyParser = require('body-parser').json();
const app = express();
//
app.use(cors());

app.use(bodyParser);

app.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swDocument))

app.use('/', router);

app.use((err, req: Request, res: Response) => { // Сделать перехватчик ошибок

  console.log({
    status: 'error',
    level: 50,
    method: req.method,
    url: req.url,
    body: req.body,
    params: req.params,
    query: req.query,
  });
  res.status(404).send({err});
});

const port = Number(process.env.APP_PORT) || 5000;

app.listen(port, () => {
  console.log(`Run port ${port} ...`);
})

module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  extends: [
    'airbnb-typescript/base', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    'prettier',
  ],
  plugins: ['@typescript-eslint', 'prettier'],
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
  },
  rules: {
    '@typescript-eslint/indent': 0,
    '@typescript-eslint/naming-convention': 0,
    'import/prefer-default-export': 0,
    'no-useless-constructor': 0, // для inject'а в конструкторах
    'no-empty-function': 0, // для inject'а в конструкторах
    'class-methods-use-this': 0,
    '@typescript-eslint/ban-ts-ignore': 0, // для динамического формирования свойств response объекта
    'max-classes-per-file': 0, // для импорта из class-validator
    'no-restricted-syntax': 0
  },
  env: {
    jest: true,
  },
};
